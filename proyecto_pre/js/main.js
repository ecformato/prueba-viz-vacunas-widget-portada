//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import * as d3 from 'd3';
import '@babel/polyfill';
import '../css/main.scss';

let selector = document.getElementById('provincias');
let chart = document.getElementById('chart');

let opcionDefecto = '20';
let data = [], filteredData = [], maxData = 0;

//Variables gnéricas del gráfico
let margin, width, height, svg, x, xAxis, y, yAxis, line;

let meses = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

//Variables
let dataVacunas = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTpRjtEFvRA-MATFdLvFcR_feCorkg84shetw4nYcsCB6Sa8mvR2-IK_blhIenJq5Tx_rudWREECLLi/pub?gid=1953409114&single=true&output=csv';

let tooltip = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-vacunas');

initData(dataVacunas);

function initData(api) {
	d3.csv(api, function(d) {
		return {
			Cod: d.Cod,
			Publicacion: d['Publicación'],
			CCAA: d.CCAA,
			Nuevas_diarias: +d.Nuevas_diarias.replace('.','').replace(',','.'),
			Media_7_dias: +d['Media_7 dias'].replace('.','').replace(',','.'),
            porc_vacunados: +d['%vacunados'].replace(',','.')
		};
		
	}, function(error,dataInit) {
		if (error) throw error;

		//Agrupamos por provincia (y a nivel nacional)
		let dataProvincias = d3.nest()
			.key(function(d) {return d.Cod;})
			.entries(dataInit);

		dataProvincias = dataProvincias.sort(comparativa);

		data = [...dataProvincias];		

		initSelector(data);
		initCharts(data, opcionDefecto);
	});
}

function initSelector(dataProvincias) {
	//Creamos las opciones del selector
	for(let i = 0; i < dataProvincias.length; i++){        
		let option = document.createElement('option');

		if(dataProvincias[i].key == '20'){
			option.text = 'España';
		} else {
			option.text = dataProvincias[i].values[0].CCAA;
		}		
		option.value = dataProvincias[i].key;

		if(opcionDefecto == dataProvincias[i].key){
			option.selected = 'true';
			opcionDefecto = dataProvincias[i].key;
		}

		if(dataProvincias[i].key == '20'){
			selector.insertBefore(option, selector.firstChild)
		} else {
			selector.appendChild(option);
		}        
	}
}

function initCharts(data, opcion) {
	//Borramos los hijos del DIV para crear el nuevo gráfico con la provincia seleccionada > Necesario para cuando hacemos el resize
    while(chart.firstChild){
        chart.removeChild(chart.firstChild);
    }

	filteredData = filteringData(data, opcion);
	maxData = filteringMaxData(filteredData);

	let t = d3.select('#chart').transition().duration(2500);

    //Barra inferior
	let randomUnaDosis = filteredData.values[filteredData.values.length - 1].porc_vacunados;
	
	document.getElementById('una-dosis').style.width = randomUnaDosis + '%';

	document.getElementById('texto-una-dosis').style.left = (randomUnaDosis + 1) + '%';
	document.getElementById('texto-una-dosis').textContent = randomUnaDosis.toFixed(1) + '%';

	//Creación del gráfico
    margin = {top: 10, right: 10, bottom: window.innerWidth < 525 ? 70 : 25, left: 75},
    width = document.getElementById('chart').clientWidth - margin.left - margin.right,
    height = document.getElementById('chart').clientHeight - margin.top - margin.bottom;

    svg = d3.select("#chart")
        .append("svg")
        .attr('id', 'histograma')
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	//Eje X >> Mejorar formato de fecha
    x = d3.scaleBand()
        .domain(filteredData.values.map(function(d) { return d.Publicacion }))
        .range([0, width])
		.padding(0.1);

    //Mostrar eje X
    let total = filteredData.values.length;
    let division5 = Math.floor(total / 4);
    let division4 = Math.floor(total / 3);
    
    xAxis = function(svg){
        svg.call(d3.axisBottom(x).tickFormat(function(d){
            let split = d.split("/");
            return parseInt(split[0]) + " " + meses[parseInt(split[1] - 1)]; 
        }).tickValues(x.domain().filter(
            function(d,i) {
                return window.innerWidth < 767 ? !(i%division4) : !(i%division5);
            })))
        svg.call(function(g) {g.selectAll('.tick').attr('text-anchor', function(d,i) { return window.innerWidth < 525 ? 'end' : i == 0 ? 'start' : i == d.length / 2 ? 'end' : 'middle'})})
        svg.call(function(g){g.selectAll('.tick line').remove()})
        svg.call(function(g){g.select('.domain').attr('id', 'eje-x-vacunas').attr('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')})
        svg.call(function(g){g.selectAll('.tick text').attr('transform', window.innerWidth < 525 ? 'rotate(-60)' : '')})
        svg.call(function(g){g.selectAll('.tick text').attr('x', window.innerWidth < 525 ? '-10' : '')})
        svg.call(function(g){g.selectAll('.tick text').attr('y', window.innerWidth < 525 ? '0' : '')});
    }        
    
    svg.append('g')
        .attr('class', 'x-axis')
        .attr('transform', 'translate(0, ' + (height + 5) +')')
        .call(xAxis);

	//Eje Y
    y = d3.scaleLinear()
        .domain([0,maxData])
        .range([height,0]);

    yAxis = function(svg){
        svg.call(d3.axisLeft(y).ticks(5).tickFormat((d) => { return getNumberWithCommas(d) }))
        svg.call(function(g){g.selectAll('.tick line')            
            .attr("stroke", "#000")
            .attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '2'})
            .attr("stroke-opacity", function(d) {return d == 0 ? '1' : '.5'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById('eje-x-vacunas').getBoundingClientRect().width) + '')
        })
        svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')})
    }

    svg.append('g')
        .attr('class', 'y-axis')
        .call(yAxis);

	//Barras
    svg.selectAll('.barras')
        .data(filteredData.values)
        .enter()
        .append('rect')
        .attr('class', 'barras-pcr')
        .attr("x", function(d) { return x(d.Publicacion); })
        .attr("y", function(d) { return y(d.Nuevas_diarias); })
        .attr("width", x.bandwidth())
        .attr("height", function(d) { return height - y(d.Nuevas_diarias); })
        .attr('fill', '#99d8c9')
        .on('touchmove mouseover', function(d,i){
            //Tooltip
            let fecha = d.Publicacion.split("/");
            fecha = fecha[0] + "/" + fecha[1];			
            
            let valorMedia = filteredData.values[i].Media_7_dias;
            
            tooltip.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltip.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><br><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithCommas(d.Nuevas_diarias) + '</span><br><span style="font-weight: 900;">Media: ' + getNumberWithCommas(valorMedia.toFixed(0).toString().replace('.',',')) + '</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 939 ? 90 : 75)) +"px")     
                .style("top", ""+ (d3.event.pageY - 75) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltip.style('display','none').style('opacity','0');
        });

	line = d3.line()
		.x(function(d) { return x(d.Publicacion) + x.bandwidth() / 2; })
        .y(function(d) { return y(d.Media_7_dias); })
        .curve(d3.curveCatmullRom.alpha(0.5));

    svg.append('path')
        .attr('class', 'linea-media')
        .datum(filteredData.values)
        .attr('fill', 'none')
        .attr('stroke-width', '2.5px')
        .attr('stroke', '#2ca25f')
        .attr('d', line);

	//Círculos ocultos (para mostrar tooltip)
    svg.selectAll('.circles')
        .data(filteredData.values)
        .enter()
        .append('circle')
        .attr('class', 'circulos-media')
        .attr("r", 6)
        .attr("cx", function(d) { return x(d.Publicacion) + x.bandwidth() / 2; })
        .attr("cy", function(d) { return y(d.Media_7_dias); })
        .style("fill", '#A7C9C7')
        .style('opacity', '0')
        .on('touchmove mouseover', function(d,i){
            //Tooltip
            let fecha = d.Publicacion.split("/");
            fecha = fecha[0] + "/" + fecha[1];

            let valorDiario = filteredData.values[i].Nuevas_diarias;
            
            tooltip.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltip.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><br><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithCommas(valorDiario) + '</span><br><span style="font-weight: 900;">Media: ' + getNumberWithCommas(d.Media_7_dias.toFixed(0).toString().replace('.',',')) + '</span>')
            .style("left", ""+ (d3.event.pageX - (window.innerWidth < 939 ? 90 : 60)) +"px")      
                .style("top", ""+ (d3.event.pageY - 75) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltip.style('display','none').style('opacity','0');
        });
}

function updateCharts(data, opcion) {
	filteredData = filteringData(data, opcion);
	maxData = filteringMaxData(filteredData);

    //Barra inferior
	let randomUnaDosis = filteredData.values[filteredData.values.length - 1].porc_vacunados;
	// let randomPautaCompleta = randomUnaDosis < 50 ? randomUnaDosis * 1.75 : randomUnaDosis * 1.25;
	// randomPautaCompleta = randomPautaCompleta > 100 ? 100 : randomPautaCompleta;
	
	document.getElementById('una-dosis').style.width = randomUnaDosis + '%';
	// document.getElementById('pauta-completa').style.width = randomPautaCompleta + '%';	

    document.getElementById('texto-una-dosis').style.left = (randomUnaDosis + 1) + '%';
	document.getElementById('texto-una-dosis').textContent = randomUnaDosis.toFixed(1) + '%';
	// document.getElementById('texto-pauta-completa').textContent = randomPautaCompleta.toFixed(1) + '%';

	let t = d3.select('#chart').transition().duration(2500);

    //Cambiamos dominio de eje Y
    y.domain([0,maxData]);
    t.select('.y-axis').call(yAxis);

	//Cambiamos datos para barras, línea y círculos
    svg.selectAll('.barras-pcr')
        .data(filteredData.values)
        .transition(t)
        .attr("x", function(d) { return x(d.Publicacion); })
        .attr("y", function(d) { return y(+d.Nuevas_diarias); })
        .attr("height", function(d) { return height - y(+d.Nuevas_diarias); });

    svg.selectAll('.linea-media')
        .datum(filteredData.values)
        .transition(t)
        .attr('d', line);

    svg.selectAll('.circulos-media')
        .data(filteredData.values)
        .transition(t)
        .attr("cy", function(d) { return y(+d.Media_7_dias); });
}

selector.addEventListener('change', (e) => {
    opcionDefecto = selector.value;
    updateCharts(data, opcionDefecto);
});

//////
// Funciones de filtrado de datos
//////
function filteringData(dataFilter, opcion){
    //Filtrados
    let newData = dataFilter.filter((item) => {
        if(item.key == opcion){
            return item;
        }
    })[0];

	console.log(newData);

	return newData;
}

function filteringMaxData(data) {
    let auxData = d3.max(data.values, (d) => { return +d.Nuevas_diarias });
	return auxData;
}

/* Helpers */
let normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};
   
    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );
   
    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
   
})();

function comparativa(a,b){
    const datoA = normalize(a.values[0].CCAA);
    const datoB = normalize(b.values[0].CCAA);

    let comparacion = 0;
    if (datoA > datoB) {
        comparacion = 1;
    } else if (datoA < datoB) {
        comparacion = -1;
    }
    return comparacion;
}

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
